/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.reportproject;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author J
 */
public class ArtistService {
    public List<ArtistReport> getTopTenArtistByTotalPrice() throws SQLException {
        ArtistDao artistDao = new ArtistDao();
        return artistDao.getArtistByTotalPrice(10);
    }
}
